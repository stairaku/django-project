In order to open the Course Recommender Interface, follow the next steps:

1. Download Django framework. Instructions to do this step could be find at: https://www.djangoproject.com/download/
2. Copy and paste the entire "MarksPrediction" folder in an specific path.
3. In command terminal, locate the "MarksPrediction" folder.
4. Type python manage.py runserver
5. On a Web Browser, insert the following URL: 127.0.0.1:8000/webapp/current_students/. 127.0.0.1:8000 is the default address Django uses to display its project.
   On 127.0.0.1:8000/webapp/current_students/, the main webpage can be found.
   On 127.0.0.1:8000/admin, the database is displayed.
