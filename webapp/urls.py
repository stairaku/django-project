from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^current_students/$', views.current_students, name = 'current_students'),
    url(r'^current_students/student_details/$', views.student_details, name = 'student_details'),
    url(r'^future_students/$', views.future_students, name = 'future_students'),
]