from django.shortcuts import render, get_list_or_404
from django.http import HttpResponse
from django.db.models import Q
from .models import Student

# Create your views here.
def current_students(request):
    return render(request, 'webapp/current_students.html')

def student_details(request):

    queryset_list = Student.objects.all()
    query = request.GET.get('q', '')

    queryset_listR = queryset_list.filter(
            Q(studentID__exact=query) &
            Q(is_predicted__exact = 'False')
        )

    queryset_listP = queryset_list.filter(
        Q(studentID__exact=query) &
        Q(is_predicted__exact = 'True')
    ).order_by('-grade')[:10]


    context = {
        "object_list1": queryset_listR,
        "object_list2": queryset_listP
    }

    return render(request, 'webapp/student_details.html', context)

def future_students(request):
    return render(request, 'webapp/future_students.html')


