from django.contrib import admin
from django.core.paginator import Paginator


# Register your models here.
from .models import Student

class StudentModelAdmin(admin.ModelAdmin):
    list_display = ["studentID", "course", "course_name", "grade", "is_predicted"]


    class Meta:
        model = Student



admin.site.register(Student, StudentModelAdmin)