from __future__ import unicode_literals
from django.db import models

#Create your models here.

class Student(models.Model):
    id = models.AutoField(verbose_name='ID', serialize= False, auto_created= True, primary_key= True)
    studentID = models.CharField(max_length =10)
    course = models.CharField(max_length = 20)
    course_name = models.CharField(max_length=100, blank = True)
    grade = models.FloatField(null = True, blank = True)
    is_predicted = models.CharField(max_length =5)

    def __unicode__(self):
        return self.studentID
